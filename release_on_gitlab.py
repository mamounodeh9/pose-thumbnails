#!/usr/bin/env python3
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import argparse
import json
import pprint
import subprocess
import urllib.parse
from typing import Any, Dict
from pathlib import Path

import requests
import requests.exceptions

gitlabURL = (
    "https://gitlab.com/api/v4/projects/blender-institute%2Fpose-thumbnails/releases"
)


def main() -> None:
    parser = argparse.ArgumentParser(description="Create a release on Gitlab.")
    parser.add_argument(
        "file_id",
        type=str,
        help="File ID of the add-on ZIP on Google Drive",
    )
    default_version = version_from_git()
    parser.add_argument(
        "--version",
        type=str,
        help=f"Version of the add-on, default is {default_version}",
        default=default_version,
    )

    args = parser.parse_args()
    access_token = load_personal_access_token()
    payload = make_request_payload(args)

    try:
        do_gitlab_request(payload, access_token)
    except requests.exceptions.HTTPError as ex:
        raise SystemExit(f"{ex}")


def version_from_git() -> str:
    """Return version based on Git tags."""

    process = subprocess.run(
        "git describe --tags --dirty --always".split(),
        stdout=subprocess.PIPE,
        check=True,
        encoding="utf8",
    )
    tag_with_version = process.stdout.strip()
    return tag_to_version(tag_with_version)


def version_to_tag(version: str) -> str:
    """Convert a tag 'version-x.y.z' to 'x.y.z'."""
    return f"version-{version}"


def tag_to_version(tag: str) -> str:
    """Convert a version 'x.y.z' to a tag 'version-x.y.z'."""
    return tag.replace("version-", "")


def make_request_payload(args: argparse.Namespace) -> str:
    """Construct the request payload for creating the release on Gitlab.

    Returns the payload as JSON string.
    """

    url = f"https://drive.google.com/uc?export=download&id={args.file_id}"
    filename = f"pose_thumbnails-{args.version}.addon.zip"

    payload = {
        "name": args.version,
        "tag_name": version_to_tag(args.version),
        "description": f"Version {args.version} of Pose Thumbnails",
        "assets": {
            "links": [
                {
                    "name": filename,
                    "url": url,
                },
            ],
        },
    }

    as_json = json.dumps(payload, indent="    ")
    print("Payload:")
    print(as_json)

    return as_json


def load_personal_access_token() -> str:
    """Load the GitLab access token from .gitlabAccessToken."""

    filepath = Path(".gitlabAccessToken")

    try:
        with filepath.open("r", encoding="utf8") as tokenfile:
            return tokenfile.read().strip()
    except IOError as ex:
        raise SystemExit(
            f"Unable to read token from {filepath}, see "
            f"https://gitlab.com/profile/personal_access_tokens\n{ex}"
        )


def do_gitlab_request(payload: str, auth_token: str) -> None:
    response = requests.post(
        url=gitlabURL,
        data=payload,
        headers={
            "Content-Type": "application/json",
            "Private-Token": auth_token,
        },
        timeout=60,
    )

    print("Response:")
    if response.headers["Content-Type"] == "application/json":
        formatted_json = json.dumps(response.json(), indent="    ")
        print(formatted_json)
    else:
        print(response.text)

    response.raise_for_status()


if __name__ == "__main__":
    main()
