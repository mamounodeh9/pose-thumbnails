# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""Code used both by creation.py and pose_thumbnails.py"""

import os.path

import bpy


def get_thumbnail_from_pose(pose: bpy.types.TimelineMarker):
    """Get the thumbnail that belongs to the pose.

    Args:
        pose (pose_marker): a pose in the pose library

    Returns:
        thumbnail PropertyGroup
    """
    if pose is None:
        return
    poselib = pose.id_data
    for thumbnail in poselib.pose_thumbnails:
        if thumbnail.frame == pose.frame:
            return thumbnail


def get_no_thumbnail_path() -> str:
    """Get the path to the 'no thumbnail' image."""
    no_thumbnail_path = os.path.join(
        os.path.dirname(__file__),
        "thumbnails",
        "no_thumbnail.png",
    )
    return no_thumbnail_path


def clear_cached_pose_thumbnails(*, full_clear=False):
    """Clear the cache of get_enum_items()."""
    from .core import get_enum_items, preview_collections

    if full_clear:
        pcoll = preview_collections["pose_library"]
        pcoll.clear()

    get_enum_items.cache_clear()
