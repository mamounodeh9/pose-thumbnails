# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
import functools


def lru_cache_1arg(wrapped):
    """Decorator, caches return value as long as the 1st arg doesn't change.

    The 1st arg MUST be a DNA datablock (e.g. have an as_pointer() function).
    """

    cached_value = ...
    cached_arg = ...

    def cache_clear():
        nonlocal cached_value, cached_arg
        cached_value = ...
        cached_arg = ...

    @functools.wraps(wrapped)
    def wrapper(*args, **kwargs):
        nonlocal cached_value, cached_arg

        if cached_value is not ... and len(args) and args[0].as_pointer() == cached_arg:
            return cached_value

        try:
            result = wrapped(*args, **kwargs)
        except:
            cache_clear()
            raise

        if len(args):
            cached_value = result
            cached_arg = args[0].as_pointer()
        else:
            cache_clear()

        return result

    wrapper.cache_clear = cache_clear
    return wrapper
