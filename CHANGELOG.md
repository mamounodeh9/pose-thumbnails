# Pose Thumbnails Changelog

This file logs the changes that are actually interesting to users (new features,
changed functionality, fixed bugs). For other changes, see the Git history.

## Version 2.2.0 (2020-11-12)

- Add explanation in the UI when thumbnails are unavailable due to the object
  not being in pose mode.
- Allow selecting pose library when using library overrides.


## Version 2.1.0 (2020-11-10)

- Compatibility fix with Blender 2.91.
- Release now made available as installable ZIP at
  https://gitlab.com/blender-institute/pose-thumbnails/-/releases


## Version 2.0.3 (2019-03-21)

- Compatibility fix with Blender 2.80 (another `user_preferences` renamed to `preferences`).
- When blending between poses, skip bones that are defined only in one pose.


## Version 2.0.2 (2018-12-18)

- Compatibility fix with Blender 2.80 (`user_preferences` renamed to `preferences`).


## Version 2.0.1 (2018-12-04)

- Compatibility fix with Blender 2.80 (only keyword parameters in layout calls).


## Version 2.0 (2018-07-13)

- Only supports Blender 2.80+
- Fixed corrupting memory when performing an undo/redo while the 'mix pose' operator is running.


## Version 1.0.2 (2018-07-03)

- Bump minimum version to 2.80 to mark as 'supports Blender 2.8'


## Version 1.0.1  (2018-06-20)

- Tweak to a caching function to prevent referencing Blender DNA
  memory.


## Version 1.0

- First release that includes this changelog. For older history, see the Git log.

## Version 1.0.1

- Added option to rename the Pose Library to match the selected character (Armature).
- Improved tooltips.
